﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CancerStatusDetection.Domain
{
    static class Examination
    {
        public static int CalculateRisk(UserData person)
        {
            int risk = 0;
            
            if(person.Age < 30)
            {
                risk += 3;
            }
            else if(person.Age <40)
            {
                risk += 4;
            }
            else //Age>40
            {
                risk += 5;
            }
            
            if(person.Education.Equals("Uneducated"))
            {
                risk += 5;
            }
            else if(person.Education.Equals("School"))
            {
                risk += 3;
            }
            else //College
            {
                risk += 2;
            }
            
            if(person.LivingArea.Equals("Urban"))
            {
                risk += 5;
            }
            else //Rural
            {
                risk += 3;
            }

            if (person.Habits.Contains("Smoking"))
            {
                risk += 3;
            }
            if (person.Habits.Contains("Alcohol"))
            {
                risk += 5;
            }
            if (person.Habits.Contains("Chewing"))
            {
                risk += 3;
            }
            if (person.Habits.Contains("Hot beverage"))
            {
                risk += 2;
            }
            if (person.Habits.Contains("Passive smoking"))
            {
                risk += 1;
            }

            if (person.OccupationHazard.Equals("Radiation Exposure"))
            {
                risk += 3;
            }
            if (person.OccupationHazard.Equals("Chemical Exposure"))
            {
                risk += 3;
            }
            if (person.OccupationHazard.Equals("Sunlight Exposure"))
            {
                risk += 2;
            }
            if (person.OccupationHazard.Equals("Thermal Exposure"))
            {
                risk += 2;
            }

            if(person.Anemia)
            {
                risk += 3;
            }
            else
            {
                risk += 1;
            }

            if (person.WeightLoss)
            {
                risk += 2;
            }
            else
            {
                risk += 1;
            }

            if(person.CancerHistory)
            {
                risk += 5;
            }
            else
            {
                risk += 1;
            }

            if(person.FFAddiction)
            {
                risk += 3;
            }

            if(person.MaritalStatus.Equals("Widowed") || person.MaritalStatus.Equals("Divorced"))
            {
                risk += 2;
            }

            if(person.NoOfChildren.Equals(">2"))
            {
                risk += 2;
            }

            return risk;
        }



        private static int NumberOfSymptoms(UserData person)
        {
            int result = person.Symptomps.Count;
            return result;
        }

        public static bool HasCancer(UserData person)
        {
            bool result = false;

            int symptomNumber = NumberOfSymptoms(person);
            if (symptomNumber == 0)
            {
                result = false;
            }
            else
            {
                result = true;
            }

            return result;
        }

        public static string CancerType(UserData person)
        {
            string result = "";

            int symptomNumber = NumberOfSymptoms(person);

            if (symptomNumber == 0)
            {
                if (person.RiskScore < 35)
                {
                    result = "No cancer";
                }
                else
                {
                    result = "You may have cancer";
                }
            }
            else if (symptomNumber == 1)
            {
                if (person.RiskScore > 35 &&
                    person.Symptomps.Contains("Severe abdominal pain or bloating"))
                {
                    result = "Stomach cancer";
                }
                else if (person.RiskScore > 35 &&
                        person.Symptomps.Contains("Swelling or mass in armpit"))
                {
                    result = "Breast cancer";
                }
                else if (person.RiskScore > 30 &&
                        person.Symptomps.Contains("Pain in chest, back, shoulder or arm"))
                {
                    result = "Lung cancer";
                }
                else if (person.RiskScore > 45 &&
                        person.Symptomps.Contains("Pain in pelvis and lower hip area"))
                {
                    result = "Cervix cancer";
                }
                else if (person.RiskScore > 30 &&
                        person.Symptomps.Contains("Ulcers in mouth or pain of teeth and jaw"))
                {
                    result = "Oral cancer";
                }
                else
                {
                    result = "Leukemia";
                }
            }
            else //more symptoms
            {
                result = "You may have leukemia";
            }

            return result;
        }
    }
}
