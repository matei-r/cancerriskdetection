﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CancerStatusDetection
{
    class ReportData
    {
        public String CancerStatus { get; set; }
        public int RiskScore { get; set; }
        public String RiskStatus { get; set; }

        public ReportData(String CancerStatus,int RiskScore,String RiskStatus)
        {
            this.CancerStatus = CancerStatus;
            this.RiskScore = RiskScore;
            this.RiskStatus = RiskStatus;
        }
    }
}
