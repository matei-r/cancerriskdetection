﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CancerStatusDetection
{
    public class UserData
    {
        public String Name { get; set; }
        public int Age { get; set; }
        public String Gender { get; set; }
        public String Education { get; set; }
        public String LivingArea { get; set; }
        public List<String> Habits { get; set; }
        public String OccupationHazard { get; set; }
        public bool Anemia;
        public bool WeightLoss;
        public bool CancerHistory;
        public String MaritalStatus { get; set; }
        public String NoOfChildren { get; set; }
        public bool FFAddiction { get; set; }
        public List<String> Symptomps { get; set; }

        //to be calculated later
        public int RiskScore { get; set; }
        public string CancerType { get; set; }

        public UserData()
        {
            this.Habits = new List<String>();
            this.Symptomps = new List<String>();
        }

        public String GetHabitsString()
        {
            String HabitsString = "";
            foreach(var Habit in Habits)
            {
                HabitsString += Habit + ",";
            }
            if (String.IsNullOrEmpty(HabitsString))
            {
                return HabitsString;
            }
            return HabitsString.Remove(HabitsString.Length - 1);
        }

        public String GetSymptomsString()
        {
            String SymptomsString = "";
            foreach (var Symptom in Symptomps)
            {
                SymptomsString += Symptom + Environment.NewLine;
            }
            return SymptomsString;
        }

        public override string ToString()
        {
            return "Name : " + this.Name + Environment.NewLine + Environment.NewLine +
                   "Age : " + (this.Age < 0 ? "N/A" : this.Age.ToString()) + Environment.NewLine + Environment.NewLine +
                   "Gender : " + this.Gender + Environment.NewLine + Environment.NewLine +
                   "Education : " + this.Education + Environment.NewLine + Environment.NewLine +
                   "Living Area : " + this.LivingArea + Environment.NewLine + Environment.NewLine +
                   "Habits : " + this.GetHabitsString() + Environment.NewLine + Environment.NewLine +
                   "Occupation Hazard : " + this.OccupationHazard + Environment.NewLine + Environment.NewLine +
                   "Anemia : " + (this.Anemia ? "Yes" : "No") + Environment.NewLine + Environment.NewLine +
                   "Weight Loss : " + (this.WeightLoss ? "Yes" : "No") + Environment.NewLine + Environment.NewLine +
                   "Family History of Cancer : " + (this.CancerHistory ? "Yes" : "No") + Environment.NewLine + Environment.NewLine +
                   "Fast Food addiction : " + (this.FFAddiction ? "Yes" : "No") + Environment.NewLine + Environment.NewLine +
                   "Marital Status : " + this.MaritalStatus + Environment.NewLine + Environment.NewLine +
                   "Number of children : " + this.NoOfChildren + Environment.NewLine + Environment.NewLine +
                   "Symptoms : " + Environment.NewLine + this.GetSymptomsString();
        }
    }
}
