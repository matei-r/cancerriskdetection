﻿using CancerStatusDetection.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CancerStatusDetection
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Gender_Loaded(object sender, RoutedEventArgs e)
        {
            List<String> genders = new List<String>();

            genders.Add("Male");
            genders.Add("Female");

            var combobox = sender as ComboBox;

            combobox.ItemsSource = genders;
        }

        private void NumberValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Education_Loaded(object sender, RoutedEventArgs e)
        {
            List<String> studies = new List<String>();

            studies.Add("Uneducated");
            studies.Add("School");
            studies.Add("College");

            var combobox = sender as ComboBox;

            combobox.ItemsSource = studies;
        }

        private void Living_area_Loaded(object sender, RoutedEventArgs e)
        {
            List<String> areas = new List<String>();

            areas.Add("Urban");
            areas.Add("Rural");

            var combobox = sender as ComboBox;

            combobox.ItemsSource = areas;

        }

        private void Habits_Loaded(object sender, RoutedEventArgs e)
        {
            List<String> habits = new List<String>();

            habits.Add("Smoking");
            habits.Add("Alcohol");
            habits.Add("Chewing");
            habits.Add("Hot baverage");
            habits.Add("Passive smoking");

            var listbox = sender as ListBox;

            listbox.ItemsSource = habits;
        }

        private void Hazards_Loaded(object sender, RoutedEventArgs e)
        {
            List<String> hazards = new List<String>();

            hazards.Add("Radiation Exposure");
            hazards.Add("Chemical Exposure");
            hazards.Add("Sunlight Exposure");
            hazards.Add("Thermal Exposure");

            var combobox = sender as ComboBox;

            combobox.ItemsSource = hazards;
        }

        public UserData GetUserData()
        {
            UserData UserData = new UserData();
            UserData.Name = Name.Text;
            int IntAge;
            bool Success = Int32.TryParse(Age.Text, out IntAge);
            if (Success)
            {
                UserData.Age = IntAge;
            } else
            {
                UserData.Age = -1;
            }
            UserData.Gender = Gender.Text;
            UserData.LivingArea = LivingArea.Text;
            UserData.Education = Education.Text;
            var habits = Habits.SelectedItems;
            foreach (var habit in habits)
            {
                UserData.Habits.Add(habit.ToString());
            }
            UserData.OccupationHazard = Hazards.Text;
            UserData.Anemia = (bool)Anemia.IsChecked;
            UserData.WeightLoss = (bool)WeightLoss.IsChecked;
            UserData.CancerHistory = (bool)CancerHistory.IsChecked;
            UserData.FFAddiction = (bool)FFAddiction.IsChecked;
            UserData.MaritalStatus = MaritalStatus.Text;
            UserData.NoOfChildren = NoOfChildren.Text;
            var symptoms = Symptoms.SelectedItems;
            foreach (var symptom in symptoms)
            {
                UserData.Symptomps.Add(symptom.ToString());
            }
            UserData.RiskScore = Examination.CalculateRisk(UserData);
            UserData.CancerType = Examination.CancerType(UserData);

            return UserData;
        }

        private void ShowData(object sender, RoutedEventArgs e)
        {
            DataWindow DataWindow = new DataWindow(GetUserData());
            DataWindow.Show();
        }

        private void ShowReport(object sender, RoutedEventArgs e)
        {
            ReportWindow ReportWindow = new ReportWindow(GetUserData());
            ReportWindow.Show();
        }

        private void Symptoms_Loaded(object sender, RoutedEventArgs e)
        {
            List<String> symptoms = new List<String>();

            symptoms.Add("Severe abdominal pain or bloating");
            symptoms.Add("Swelling or mass in armpit");
            symptoms.Add("Pain in chest, back, shoulder or arm");
            symptoms.Add("Pain in pelvis and lower hip area");
            symptoms.Add("Ulcers in mouth or pain of teeth and jaw");
            symptoms.Add("Other");

            var listbox = sender as ListBox;

            listbox.ItemsSource = symptoms;
        }

        private void MaritalStatus_Loaded(object sender, RoutedEventArgs e)
        {
            List<String> mstatuses = new List<String>();

            mstatuses.Add("Single");
            mstatuses.Add("Married");
            mstatuses.Add("Divorced");
            mstatuses.Add("Widowed");

            var combobox = sender as ComboBox;

            combobox.ItemsSource = mstatuses;
        }

        private void NoOfChildren_Loaded(object sender, RoutedEventArgs e)
        {
            List<String> noofchildren = new List<String>();

            noofchildren.Add("0");
            noofchildren.Add("1-2");
            noofchildren.Add(">2");

            var combobox = sender as ComboBox;

            combobox.ItemsSource = noofchildren;
        }
    }
}
