﻿using CancerStatusDetection.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CancerStatusDetection
{
    /// <summary>
    /// Interaction logic for ReportWindow.xaml
    /// </summary>
    public partial class ReportWindow : Window
    {
        private UserData userData;

        public ReportWindow(UserData user)
        {
            InitializeComponent();
            this.userData = user;
        }
        

        private void CloseWindow(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if(Examination.HasCancer(userData))
            {
                CancerStatus.Content = "Suspect of cancer";
            }
            else
            {
                CancerStatus.Content = "Cancer free";
            }

            RiskScore.Content = userData.RiskScore;

            RiskStatus.Content = userData.CancerType;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if ((string)CancerStatus.Content == "Cancer free" &&
                userData.RiskScore < 35)
            {
                MessageBox.Show("Do simple clinical tests to confirm", "Recommendation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if ((string)CancerStatus.Content == "Cancer free" &&
                userData.RiskScore >= 35)
            {
                MessageBox.Show("Do blood test and x ray to confirm", "Recommendation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            else if ((string)CancerStatus.Content == "Suspect of cancer" &&
                (string)RiskStatus.Content == "Stomach cancer")
            {
                MessageBox.Show("Do endoscopy of stomach", "Recommendation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if ((string)CancerStatus.Content == "Suspect of cancer" &&
                (string)RiskStatus.Content == "Breast cancer")
            {
                MessageBox.Show("Do mammogram and PET scan of breast", "Recommendation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if ((string)CancerStatus.Content == "Suspect of cancer" &&
                (string)RiskStatus.Content == "Lung cancer")
            {
                MessageBox.Show("Do CT scan of chest", "Recommendation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if ((string)CancerStatus.Content == "Suspect of cancer" &&
                (string)RiskStatus.Content == "Cervix cancer")
            {
                MessageBox.Show("Do pap smear test", "Recommendation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if ((string)CancerStatus.Content == "Suspect of cancer" &&
                (string)RiskStatus.Content == "Oral cancer")
            {
                MessageBox.Show("Do biopsy of tongue and inner mouth", "Recommendation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            else
            {
                MessageBox.Show("Do biopsy of bone marrow", "Recommendation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
